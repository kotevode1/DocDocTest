//
//  SpecialtiesTableViewController.swift
//  DocDocTest
//
//  Created by Mark on 22.05.2018.
//  Copyright © 2018 Mark. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class SpecialtiesTableViewController: UITableViewController {
    
    private struct Constants {
        static let CellID = "Cell"
        static let DataURL = "https://api.docdoc.ru/public/rest/1.0.9/speciality"
        static let APILogin = "partner.13703"
        static let APIPassword = "ZZdFmtJD"
    }
    
    var specialties = [Specialty]() {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return specialties.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellID, for: indexPath)
        cell.textLabel?.text = specialties[indexPath.row].namePlural
        return cell
    }

    private func fetchData() {
        Alamofire.request(Constants.DataURL)
            .authenticate(user: Constants.APILogin, password: Constants.APIPassword)
            .responseArray(keyPath: "SpecList") { (response: DataResponse<[Specialty]>) in
                switch response.result {
                case .success(let data):
                    self.specialties = data
                case .failure(let error):
                    debugPrint(error)
                }
        }
    }

}

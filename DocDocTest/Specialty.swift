//
//  Specialty.swift
//  DocDocTest
//
//  Created by Mark on 22.05.2018.
//  Copyright © 2018 Mark. All rights reserved.
//

import Foundation
import ObjectMapper

class Specialty: Mappable {
    var name: String?
    var namePlural: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name <- map["Name"]
        namePlural <- map["NamePlural"]
    }
}
